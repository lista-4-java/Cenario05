package br.edu.up.view;

import br.edu.up.utilits.Prompt;
import br.edu.up.controls.*;

public class View {

    Controle controle = new Controle();

    public void msg(){
    Prompt.imprimir("---------------EVENTOS------------------");
    Prompt.imprimir("");
    Prompt.imprimir("escolha uma opcao: ");
    Prompt.imprimir("1 - adicionar evento.");
    Prompt.imprimir("2 - listar eventos.");
    Prompt.imprimir("3 - realizar rezerva.");
    Prompt.imprimir("4 - listar reserva.");
    Prompt.imprimir("5 - sair");
    }

    

    public void Controlar() {

        int opcao = Prompt.lerInteiro("opcao: ");

        switch (opcao) {
            case 1:
                Prompt.imprimir("------------------ADICIONAR EVENTO-------------------------");
                String nome = Prompt.lerLinha("Digite o nome do evento:");
                String data = Prompt.lerLinha("Digite a data do evento:");
                String local = Prompt.lerLinha("Digite o local do evento:");
                int lotacaoMax = Prompt.lerInteiro("Digite a lotação máxima do evento:");
                int qtdeIngressosVendidos = Prompt.lerInteiro("Digite a quantidade de ingressos vendidos:");
                double precoIngresso = Prompt.lerDecimal("Digite o preço do ingresso:");

                controle.CriarEvento(nome, data, local, lotacaoMax, qtdeIngressosVendidos, precoIngresso);

                msg();
                Controlar();
                break;
            case 2:
                Prompt.imprimir("----------------LISTAR EVENTO----------------------");
                Prompt.imprimir(controle.listarEventos());

                msg();
                Controlar();
                break;
            case 3:
                Prompt.imprimir("----------------ADICIONAR RESERVA--------------------");

                Prompt.imprimir("");
                Prompt.imprimir("ingressos disponiveis: " + controle.mostrarQtdeDeingressos());

                String responsavel = Prompt.lerLinha("Digite o nome do responsável pela reserva:");
                int quantidadePessoas = Prompt.lerInteiro("Digite a quantidade de pessoas:");

                controle.realizarReserva(responsavel, quantidadePessoas);
                controle.validarQtdePessoa(quantidadePessoas);

                Prompt.imprimir("ingressos foram vendidos? : " + controle.mostrarMsg());
                Prompt.imprimir("ingressos disponiveis: " + controle.mostrarQtdeDeingressos());

                msg();
                Controlar();
                break;
            case 4:
                Prompt.imprimir("----------------LISTAR RESERVA-----------------------");
                Prompt.imprimir(controle.listarReservas());

                msg();
                Controlar();
                break;
            case 5:
                Prompt.imprimir("Saindo...");
                break;
            default:
                Prompt.imprimir("Opção invál");
                msg();
                Controlar();
        }
    }
}
