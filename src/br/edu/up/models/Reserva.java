package br.edu.up.models;
public class Reserva {
    private String responsavel;
    private int quantidadePessoa;
    private String dataReserva;
    private double valorTotal;
    private String nomeResponsavel;
    private int qtdePessoas;
    private Evento[] eventos;


    public Reserva(String responsavel, int quantidadePessoas) {
        this.responsavel = responsavel;
        this.qtdePessoas = quantidadePessoas;

    }
    
     
    public String getResponsavel() {
        return responsavel;
    }



    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }



    public int getQuantidadePessoa() {
        return quantidadePessoa;
    }



    public void setQuantidadePessoa(int quantidadePessoa) {
        this.quantidadePessoa = quantidadePessoa;
    }



    public String getDataReserva() {
        return dataReserva;
    }


    public void setDataReserva(String dataReserva) {
        this.dataReserva = dataReserva;
    }


    public void receberData(Evento eventos) {
        setDataReserva(eventos.getData());
    }



    public double getValorTotal() {
        return valorTotal;
    }



    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }



    public String getNomeResponsavel() {
        return nomeResponsavel;
    }



    public void setNomeResponsavel(String nomeResponsavel) {
        this.nomeResponsavel = nomeResponsavel;
    }



    public int getQtdePessoas() {
        return qtdePessoas;
    }



    public void setQtdePessoas(int qtdePessoas) {
        this.qtdePessoas = qtdePessoas;
    }


    public void valorTotal(Evento eventos){
        valorTotal = eventos.getPrecoIngresso() * getQtdePessoas(); 
    }

    @Override
    public String toString() {
        return "Reserva{" +
                "responsavel='" + responsavel + '\'' +
                ", quantidadePessoas=" + qtdePessoas +
                ", dataReserva='" + getDataReserva() + '\'' +
                ", valorTotal=" + valorTotal +
                '}';
    }
}
