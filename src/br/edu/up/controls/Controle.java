package br.edu.up.controls;

import br.edu.up.models.Reserva;
import br.edu.up.models.Evento;

public class Controle {

    private boolean validar;
    private String msg;
    private Evento[] eventos = new Evento[1];
    private Reserva[] reservas = new Reserva[1];

    public void CriarEvento(String nome, String data, String local, int lotacaoMax, int qtdeIngressosVendidos,Double precoIngresso) {

        eventos[0] = new Evento(nome, data, local, lotacaoMax, qtdeIngressosVendidos, precoIngresso);

    }

    public String listarEventos() {
       return  eventos[0].toString();
    }

    public void realizarReserva(String responsavel, int quantidadePessoas) {

            reservas[0] = new Reserva(responsavel, quantidadePessoas);
            reservas[0].receberData(eventos[0]);
            reservas[0].valorTotal(eventos[0]);

    }

    public String listarReservas() {
        return reservas[0].toString(); 
    }

    public int mostrarQtdeDeingressos(){
        return eventos[0].getIngressosDisponiveis();
    }


    
    public void validarQtdePessoa(int qtdePessoas){
 
        if( eventos[0].getIngressosDisponiveis() >= qtdePessoas){
             validar = true;
        }
        else{
            validar = false;
        }

        if(validar == true){
            eventos[0].setQtdIngrassosVendido((eventos[0].getQtdeIngressosVendidos() + qtdePessoas));
        }
        else{
            eventos[0].getQtdeIngressosVendidos();
        }
    }

    public String mostrarMsg(){

        if(validar == true){
           return  msg = "ingressos vendidos com sucesso";
        }
        else{
            return msg = "ingressos insuficientes, criar nova reserva.";
        }

    }

}