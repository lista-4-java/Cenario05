package br.edu.up.models;

public class Evento {
   private String nome;
   private String data;
   private String local;
   private int lotacaoMax;
   private int qtdeIngressosVendidos;
   private Double precoIngresso;

public Evento(String nome, String data, String local, int lotacaoMax, int qtdeIngressosVendidos, Double precoIngresso){
   this.nome = nome;
   this.data = data;
   this.local = local;
   this.lotacaoMax = lotacaoMax;
   this.qtdeIngressosVendidos = qtdeIngressosVendidos;
   this.precoIngresso = precoIngresso;
}

 public void setQtdIngrassosVendido(int qtdeIngressosVendidos){
   this.qtdeIngressosVendidos = qtdeIngressosVendidos;
 }

public String getNome() {
   return nome;
}

public void setNome(String nome) {
   this.nome = nome;
}

public String getData() {
   return data;
}

public void setData(String data) {
   this.data = data;
}

public String getLocal() {
   return local;
}

public void setLocal(String local) {
   this.local = local;
}

public int getLotacaoMax() {
   return lotacaoMax;
}

public void setLotacaoMax(int lotacaoMax) {
   this.lotacaoMax = lotacaoMax;
}

public int getQtdeIngressosVendidos() {
   return qtdeIngressosVendidos;
}

public void setQtdeIngressosVendidos(int qtdeIngressosVendidos) {
   this.qtdeIngressosVendidos = qtdeIngressosVendidos;
}

public Double getPrecoIngresso() {
   return precoIngresso;
}

public void setPrecoIngresso(Double precoIngresso) {
   this.precoIngresso = precoIngresso;
}

public Integer getIngressosDisponiveis() {
   Integer  aux = lotacaoMax - qtdeIngressosVendidos;

        if(aux <= 0){
           return  0;
        }
        else{
            return aux;
        }
}


public void precoIngressoTotal(){

}

@Override
public String toString() {
    return "Evento{" +
            "nome='" + nome + '\'' +
            ", data='" + data + '\'' +
            ", local='" + local + '\'' +
            ", lotacaoMaxima=" + lotacaoMax +
            ", ingressosVendidos=" + qtdeIngressosVendidos +
            ", precoIngresso=" + precoIngresso +
            ", ingressosDisponiveis=" + getIngressosDisponiveis() +
            '}';
}

   
}